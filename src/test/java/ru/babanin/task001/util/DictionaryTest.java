package ru.babanin.task001.util;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DictionaryTest {
    @Test
    void getUniqueWords() {
        Dictionary dictionary = Dictionary.getInstance();

        String[] keys = {"мама", "мыла", "раму"};
        for (String s : keys) {
            dictionary.accept(s);
        }

        List<String> list = dictionary.getUniqueWords();
        assertEquals(list.size(), 3);
        assertTrue(list.contains("мама"));
        assertTrue(list.contains("мыла"));
        assertTrue(list.contains("раму"));

        dictionary.reset();


        String[] keys2 = {"мама", "мыла", "раму", "мама", "раму"};
        for (String s : keys2) {
            dictionary.accept(s);
        }

        list = dictionary.getUniqueWords();
        assertEquals(list.size(), 1);
        assertFalse(list.contains("мама"));
        assertTrue(list.contains("мыла"));
        assertFalse(list.contains("раму"));

        dictionary.reset();
    }
}