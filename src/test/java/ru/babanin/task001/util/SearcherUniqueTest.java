package ru.babanin.task001.util;

import org.junit.jupiter.api.Test;
import ru.babanin.task001.printer.Printer;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SearcherUniqueTest {
    @Test
    void search() {
        try {
            String[] args = {"test_data/junit/good.txt" , "test_data/junit/good2.txt", "test_data/junit/good3.txt"};
            List<String> list = SearcherUnique.Search(args);
            assertEquals(list.size(), 164);
        } catch (SearcherUniqueException e) {
            assertTrue(false);
        }

        try {
            String[] args = {"test_data/junit/good.txt", "test_data/junit/good.txt"};
            List<String> list = SearcherUnique.Search(args);
            assertTrue(list.isEmpty());
        } catch (SearcherUniqueException e) {
            assertTrue(false);
        }

        try {
            String[] args = {"test_data/junit/good.txt", "test_data/junit/good.txt", "test_data/junit/good.txt", "test_data/junit/good.txt", "test_data/junit/good.txt", "test_data/junit/good.txt", "test_data/junit/good.txt", "test_data/junit/good.txt"};
            List<String> list = SearcherUnique.Search(args);
            assertTrue(list.isEmpty());
        } catch (SearcherUniqueException e) {
            assertTrue(false);
        }

        try {
            String[] args = {"test_data/junit/good.txt", "test_data/junit/bad_char.txt"};
            List<String> list = SearcherUnique.Search(args);
            assertTrue(false);
        } catch (SearcherUniqueException e) {
            assertTrue(true);
        }

        try {
            String[] args = {"test_data/junit/good.txt", "test_data/junit/bad_word.txt"};
            List<String> list = SearcherUnique.Search(args);
            assertTrue(false);
        } catch (SearcherUniqueException e) {
            assertTrue(true);
        }
    }

}