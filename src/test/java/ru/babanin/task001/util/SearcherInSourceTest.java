package ru.babanin.task001.util;

import org.junit.jupiter.api.Test;
import ru.babanin.task001.parser.Parser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

class SearcherInSourceTest {
    @Test
    void getTypeFileFromName() {
        assertTrue(SearcherInSource.GetTypeFileFromName("file.txt") == TypeSources.LOCAL_FILE);
        assertTrue(SearcherInSource.GetTypeFileFromName("C:\\\\file.txt") == TypeSources.LOCAL_FILE);

        assertFalse(SearcherInSource.GetTypeFileFromName("file.dat") == TypeSources.LOCAL_FILE);

        assertTrue(SearcherInSource.GetTypeFileFromName("https://vk.com/doc11336279_441871852") == TypeSources.URL_FILE);

        assertFalse(SearcherInSource.GetTypeFileFromName("https://vk.com/") == TypeSources.URL_FILE);
        assertFalse(SearcherInSource.GetTypeFileFromName("https://vk.com") == TypeSources.URL_FILE);
        assertFalse(SearcherInSource.GetTypeFileFromName("https://test.txt") == TypeSources.URL_FILE);
    }

    class SearcherFake extends SearcherInSource{
        private List<String> list = new ArrayList<>();

        SearcherFake() {
            super("fake", null, new BoxBadFlag());
        }

        @Override
        public void run() { }

        void testRun(String test) throws IOException{
            try (Parser parser = new Parser(new ByteArrayInputStream(test.getBytes(StandardCharsets.UTF_8)))) {
                readAll(parser);
            }
        }

        @Override
        void moveWord(String s) {
            list.add(s);
        }

        List<String> getList() {
            return list;
        }
    }

    @Test
    void readAll() {
        try {
            String test1 = "Это нормальная сторока содержащия 5 слов.";
            SearcherFake searcher = new SearcherFake();
            searcher.testRun(test1);

            List<String> list = searcher.getList();

            assertTrue(list.size() == 5);
            assertTrue(list.contains("это".intern()));
            assertTrue(list.contains("нормальная"));
            assertTrue(list.contains("сторока"));
            assertTrue(list.contains("содержащия"));
            assertTrue(list.contains("слов"));
            assertFalse(list.contains("5"));
        } catch (Exception e) {assertTrue(false);}

        try {
            String test1 = "12 21, !? 999. - ---, ";
            SearcherFake searcher = new SearcherFake();
            searcher.testRun(test1);

            List<String> list = searcher.getList();
            assertTrue(list.size() == 0);
        } catch (Exception e) {
            assertTrue(false);
        }

        try {
            String test1 = "тут бросается exception должно быть два слова";
            SearcherFake searcher = new SearcherFake();
            searcher.testRun(test1);

            List<String> list = searcher.getList();
            assertTrue(list.size() == 2);
            assertTrue(list.contains("тут"));
            assertTrue(list.contains("бросается"));
            assertFalse(list.contains("exception"));
        } catch (Exception e) {assertTrue(false);}
    }

}