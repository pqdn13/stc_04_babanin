package ru.babanin.task001.parser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {
    private final char[] specialChars = {'–', '\n', '\r', '\t', ' ', '.', ',', '!', '?', ':', ';', '-', '(', '[', '{', ')', ']', '}', '\"', '«', '»', '-'};
    private final char russianChars[] = "йцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮЁё".toCharArray();
    private final char englishChars[] = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM".toCharArray();

    @org.junit.jupiter.api.Test
    void nextWord() {
        String goodString = "Мама мыла раму, а папа тем временем работал веб-программистом и это круто!!! :)";

        try (Parser parser = new Parser(new ByteArrayInputStream(goodString.getBytes(StandardCharsets.UTF_8)))) {
            while (true) {
                String s = parser.nextWord();
                if(s == null) break;
            }
        } catch (IOException e) {
            assertTrue(false);
        } catch (BadRussianWordException e) {
            assertTrue(false);
        } catch (BadCharInputException e) {
            assertTrue(false);
        }

        String badChars = "Мама мыла раму, а father тем временем работал веб-программистом и это круто!!! :)";

        try (Parser parser = new Parser(new ByteArrayInputStream(badChars.getBytes(StandardCharsets.UTF_8)))) {
            while (true) {
                String s = parser.nextWord();
                if(s == null) break;
            }
            assertTrue(false);
        } catch (IOException e) {
            assertTrue(false);
        } catch (BadRussianWordException e) {
            assertTrue(false);
        } catch (BadCharInputException e) {
            assertTrue(true);  // get exception
        }

        String badValidWord = "Мама мыла раму, а папа тем временем работал веб-про-гра-ммистом и это круто!!! :)";

        try (Parser parser = new Parser(new ByteArrayInputStream(badValidWord.getBytes(StandardCharsets.UTF_8)))) {
            while (true) {
                String s = parser.nextWord();
                if(s == null) break;
            }
            assertTrue(false);
        } catch (IOException e) {
            assertTrue(false);
        } catch (BadRussianWordException e) {
            assertTrue(true); // get exception
        } catch (BadCharInputException e) {
            assertTrue(false);
        }

    }

    @org.junit.jupiter.api.Test
    void checkWord() {
        try {
            Parser.checkWord("мама");
        } catch (BadRussianWordException e) {
            assertTrue(false);
        }

        try {
            Parser.checkWord("веб-программист");
        } catch (BadRussianWordException e) {
            assertTrue(false);
        }

        try {
            Parser.checkWord("mather");
            assertTrue(false);
        } catch (BadRussianWordException e) {
            assertTrue(true);
        }

        try {
            Parser.checkWord("мама-");
            assertTrue(false);
        } catch (BadRussianWordException e) {
            assertTrue(true);
        }

        try {
            Parser.checkWord("77мама");
            assertTrue(false);
        } catch (BadRussianWordException e) {
            assertTrue(true);
        }

        try {
            Parser.checkWord("123");
            assertTrue(false);
        } catch (BadRussianWordException e) {
            assertTrue(true);
        }
    }

    @org.junit.jupiter.api.Test
    void isValidRussianWord() {
        assertTrue(Parser.isValidRussianWord("галилео"));
        assertTrue(Parser.isValidRussianWord("ТУМАННОСТЬ"));
        assertTrue(Parser.isValidRussianWord("АЛЬбиоНА"));
        assertTrue(Parser.isValidRussianWord("веб-программист"));

        assertFalse(Parser.isValidRussianWord("галилео-"));
        assertFalse(Parser.isValidRussianWord("-галилео"));
        assertFalse(Parser.isValidRussianWord("-галилео-"));
        assertFalse(Parser.isValidRussianWord("уха-ха-хо-мо"));

        assertFalse(Parser.isValidRussianWord("123"));
        assertFalse(Parser.isValidRussianWord("АЛЬби123оНА"));
        assertFalse(Parser.isValidRussianWord("класс.роботов"));
        assertFalse(Parser.isValidRussianWord("!@^^&"));

        assertFalse(Parser.isValidRussianWord("yes"));
    }

    @org.junit.jupiter.api.Test
    void normalizationWord() {
        assertTrue("галилео".equals(Parser.normalizationWord("галилео")));

        assertTrue("мама".equals(Parser.normalizationWord("мама-")));
        assertTrue("мама".equals(Parser.normalizationWord("-мама")));
        assertTrue("мама".equals(Parser.normalizationWord("-мама-")));

        assertTrue("веб-программист".equals(Parser.normalizationWord("веб-программист")));
        assertTrue("веб-программист".equals(Parser.normalizationWord("веб-программист-----")));
        assertTrue("веб-программист".equals(Parser.normalizationWord("-------веб-программист")));

        assertFalse("мама".equals(Parser.normalizationWord("веб-программист")));
        assertFalse("мама".equals(Parser.normalizationWord(null)));
    }

    @org.junit.jupiter.api.Test
    void checkChar() {
        for (char c : russianChars) {
            try {
                Parser.checkChar(c);
            } catch (BadCharInputException e) {
                assertTrue(false);
            }
        }

        for (char c : specialChars) {
            try {
                Parser.checkChar(c);
            } catch (BadCharInputException e) {
                assertTrue(false);
            }
        }

        for (char c : englishChars) {
            try {
                Parser.checkChar(c);
                assertTrue(false);
            } catch (BadCharInputException e) {
                assertTrue(true);
            }
        }
    }

    @org.junit.jupiter.api.Test
    void isOkChar() {
        for (char c : specialChars) {
            assertTrue(Parser.isOkChar(c));
        }

        for (char c : russianChars) {
            assertTrue(Parser.isOkChar(c));
        }

        for (char c : englishChars) {
            assertFalse(Parser.isOkChar(c));
        }
    }

    @org.junit.jupiter.api.Test
    void isRussianLetter() {
        for (char c : russianChars) {
            assertTrue(Parser.isRussianLetter(c));
        }

        char badChars[] = "123`~!@#$%^&*()_+=-\\|/?.,".toCharArray();
        for (char c : badChars) {
            assertFalse(Parser.isRussianLetter(c));
        }
    }

}