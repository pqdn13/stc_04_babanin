DROP TABLE people CASCADE;
DROP TABLE security CASCADE;
DROP TABLE tests CASCADE;
DROP TABLE questions CASCADE;
DROP TABLE pass_test CASCADE;
DROP TABLE total_tests CASCADE;

CREATE TABLE people (
  ID         SERIAL PRIMARY KEY,
  name       VARCHAR(80),
  birth      DATE,
  check_in   DATE,
  last_edit  DATE,
  email      VARCHAR(60),
  inno_email VARCHAR(60)
);

CREATE TABLE security (
  ID            SERIAL PRIMARY KEY,
  id_person     BIGINT,
  login         VARCHAR(60),
  hash_password BIGINT,
  access_level  SMALLINT,
  is_activate   BOOLEAN,
  FOREIGN KEY (id_person) REFERENCES people (ID)
);

CREATE TABLE tests (
  ID   SERIAL PRIMARY KEY,
  name VARCHAR(60)
);

CREATE TABLE questions (
  ID         SERIAL PRIMARY KEY,
  name       VARCHAR(60),
  text       TEXT,
  criterions TEXT,
  max_mark   INT
);

CREATE TABLE total_tests (
  ID       SERIAL PRIMARY KEY,
  id_test  BIGINT,
  id_quest BIGINT,
  FOREIGN KEY (id_test) REFERENCES tests (ID),
  FOREIGN KEY (id_quest) REFERENCES questions (ID)
);

CREATE TABLE pass_test (
  ID          SERIAL PRIMARY KEY,
  id_person   BIGINT,
  id_quest    BIGINT,
  id_test     BIGINT,
  mark        INT,
  is_tested BOOLEAN,
  FOREIGN KEY (id_person) REFERENCES people (ID),
  FOREIGN KEY (id_quest) REFERENCES questions (ID),
  FOREIGN KEY (id_test) REFERENCES tests (ID)
);


INSERT INTO people (id_student, id_lesson, date) VALUES (3, 3, '2017-02-17');











