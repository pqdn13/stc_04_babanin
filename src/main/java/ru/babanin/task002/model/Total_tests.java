package ru.babanin.task002.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Total_tests {
  private Long id;
  private Long id_test;
  private Long id_quest;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId_test() {
    return id_test;
  }

  public void setId_test(Long id_test) {
    this.id_test = id_test;
  }

  public Long getId_quest() {
    return id_quest;
  }

  public void setId_quest(Long id_quest) {
    this.id_quest = id_quest;
  }
}
