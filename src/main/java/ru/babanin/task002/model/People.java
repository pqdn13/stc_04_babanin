package ru.babanin.task002.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class People {
  private Long id;
  private String name;
  private java.sql.Date birth;
  private java.sql.Date check_in;
  private java.sql.Date last_edit;
  private String email;
  private String inno_email;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public java.sql.Date getBirth() {
    return birth;
  }

  public void setBirth(java.sql.Date birth) {
    this.birth = birth;
  }

  public java.sql.Date getCheck_in() {
    return check_in;
  }

  public void setCheck_in(java.sql.Date check_in) {
    this.check_in = check_in;
  }

  public java.sql.Date getLast_edit() {
    return last_edit;
  }

  public void setLast_edit(java.sql.Date last_edit) {
    this.last_edit = last_edit;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getInno_email() {
    return inno_email;
  }

  public void setInno_email(String inno_email) {
    this.inno_email = inno_email;
  }
}
