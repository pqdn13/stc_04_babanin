package ru.babanin.task002.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Questions {
  private Long id;
  private String name;
  private String text;
  private String criterions;
  private Long max_mark;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getCriterions() {
    return criterions;
  }

  public void setCriterions(String criterions) {
    this.criterions = criterions;
  }

  public Long getMax_mark() {
    return max_mark;
  }

  public void setMax_mark(Long max_mark) {
    this.max_mark = max_mark;
  }
}
