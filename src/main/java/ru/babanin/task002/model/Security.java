package ru.babanin.task002.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Security {
  private Long id;
  private Long id_person;
  private String login;
  private Long hash_password;
  private Long access_level;
  private String is_activate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getId_person() {
    return id_person;
  }

  public void setId_person(Long id_person) {
    this.id_person = id_person;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public Long getHash_password() {
    return hash_password;
  }

  public void setHash_password(Long hash_password) {
    this.hash_password = hash_password;
  }

  public Long getAccess_level() {
    return access_level;
  }

  public void setAccess_level(Long access_level) {
    this.access_level = access_level;
  }

  public String getIs_activate() {
    return is_activate;
  }

  public void setIs_activate(String is_activate) {
    this.is_activate = is_activate;
  }
}
