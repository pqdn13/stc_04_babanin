package ru.babanin.task001.parser;

import java.io.IOException;

/**
 * Created by makcim on 23.02.17.
 */
public interface Supplier {
    int get() throws IOException;
}
