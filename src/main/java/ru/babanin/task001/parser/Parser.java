package ru.babanin.task001.parser;

import java.io.*;

/**
 * Обработчик слов из некоторого входного потока
 */
public class Parser implements Closeable{
    Supplier supplier;
    private BufferedReader reader;
    private StringBuilder buffer = new StringBuilder();
    private final static char[] specialChars = {'–', '\n', '\r', '\t', ' ', '.', ',', '!', '?', ':', ';', '-', '(', '[', '{', ')', ']', '}', '\"', '«', '»', '-'};

    /**
     * Инициализация парсера некоторым входным потоком
     */
    public Parser(InputStream inputStream) {
        this.reader = new BufferedReader(new InputStreamReader(inputStream));
        supplier = ()->{
          return reader.read();
        };
    }

    public Parser(Supplier supplier) {
        this.reader = null;
        this.supplier = supplier;
    }

    /**
     * Парсит входной поток, находя в нём русские слова
     *
     * @return Возвращает русское слово, null если парсер корректно отработал
     * @throws IOException Ошибка чтения входного потока
     * @throws BadCharInputException  Недопустимый символ во входном потоке
     * @throws BadRussianWordException Некоректное слово
     */
    public String nextWord() throws IOException, BadCharInputException, BadRussianWordException{
        if(reader == null) return null;

        String out = null;
        while (true) {
            int data = supplier.get();
            if (data == -1){
                if (buffer.length() > 0) {
                    out = buffer.toString().toLowerCase();
                    if (isDash(buffer.toString())) {
                        out = null;
                    }
                }
                break;
            }

            char ch = (char) data;
            checkChar(ch);

            if (isRussianLetter(ch) || ch == '-') {
                buffer.append(ch);
            }else{
                if (buffer.length() > 0) {
                    out = buffer.toString().toLowerCase();
                    if (isDash(out)) {
                        out = null;
                        buffer.setLength(0);
                    }else {
                        break;
                    }
                }
            }
        }

        buffer.setLength(0);
        out = normalizationWord(out);
        if(out != null) checkWord(out);
        return out;
    }

    private static boolean isDash(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != '-') {
                return false;
            }
        }
        return true;
    }

    static void checkWord(String word) throws  BadRussianWordException{
        if (!isValidRussianWord(word)){
            throw new BadRussianWordException("Bad word: [" + word + "]");
        }
    }

    static boolean isValidRussianWord(String word) {
        if (word == null) {
            return false;
        }

        if (word.length() == 0) {
            return false;
        }

        if (word.charAt(0) == '-' || word.charAt(word.length() - 1) == '-') {
            return false;
        }

        for (int i = 0; i < word.length(); i++) {
            if( !(isRussianLetter(word.charAt(i)) || word.charAt(i) == '-') ) return false;
        }

        int count = 0;
        for (int i = 0; i < word.length(); i++) {
            if(word.charAt(i) == '-') count++;
        }

        return count < 2;
    }

    static String normalizationWord(String word) {
        if (word == null) {
            return word;
        }

        int left = 0;
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) != '-') {
                left = i;
                break;
            }
        }

        int right = word.length();
        for (int i = word.length()-1; i >=0; i--) {
            if (word.charAt(i) != '-') {
                right = i;
                break;
            }
        }

        return word.substring(left, right + 1);
    }

    static void checkChar(char ch) throws  BadCharInputException{
        if (!isOkChar(ch)){
            throw new BadCharInputException(Character.isLetter(ch) ? ("not russian letter [" + ch + ']') : ("unknown char [" + ch + "]"));
        }
    }

    static boolean isOkChar(char ch) {
        if(isRussianLetter(ch) || Character.isDigit(ch)) return true;
        
        boolean isFind = false;
        for (int i = 0; i < specialChars.length; i++) {
            if(specialChars[i] == ch) return true;
        }

        return isFind;
    }

    static boolean isRussianLetter(char ch) {
        return ((ch >= 'а' && ch <= 'я') || ((ch >= 'А' && ch <= 'Я') || ch == 'Ё' || ch == 'ё'));
    }

    @Override
    public void close() throws IOException {
        if (reader != null) {
            reader.close();
        }
    }
}
