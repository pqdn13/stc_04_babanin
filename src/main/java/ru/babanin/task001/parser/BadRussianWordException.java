package ru.babanin.task001.parser;

/**
 * Не валидное русское слово
 */
public class BadRussianWordException extends Exception {
    BadRussianWordException(String s) {
        super(s);
    }
}
