package ru.babanin.task001.parser;

/**
 * Не валидный входной символ
 */
public class BadCharInputException extends Exception{
    BadCharInputException(String s) {
        super(s);
    }
}
