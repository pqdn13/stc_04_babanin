package ru.babanin.task001.util;

/**
 * Сброс настроек класса
 */
public interface Resetable {
    void reset();
}
