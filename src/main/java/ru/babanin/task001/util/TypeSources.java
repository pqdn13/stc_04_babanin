package ru.babanin.task001.util;

/**
 * Типы поддерживаемых источников
 */
public enum TypeSources {
    LOCAL_FILE, URL_FILE, UNKNOWN
}
