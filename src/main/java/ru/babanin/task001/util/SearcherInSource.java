package ru.babanin.task001.util;

import ru.babanin.task001.parser.BadCharInputException;
import ru.babanin.task001.parser.BadRussianWordException;
import ru.babanin.task001.parser.Parser;
import ru.babanin.task001.printer.Printer;

import java.io.IOException;
import java.util.function.Consumer;
import java.util.regex.Pattern;

abstract class SearcherInSource implements Runnable {
    String source;
    private Consumer<String> dict;
    private BoxBadFlag badFlag;
    private Printer printer;

    SearcherInSource(String source, Consumer<String> consumer, BoxBadFlag badFlag) {
        this.source = source;
        this.dict = consumer;
        this.badFlag = badFlag;
        printer = Printer.getInstance();
    }

    static TypeSources GetTypeFileFromName(String source) {
        TypeSources type = TypeSources.UNKNOWN;

        Pattern urlFilePattern = Pattern.compile("[\\w\\.]+:\\/\\/\\S+\\/\\S+");
        if(urlFilePattern.matcher(source).find()){
            type = TypeSources.URL_FILE;
        }else {
            Pattern urlPattern = Pattern.compile("[\\w\\.]+:\\/\\/\\S+");
            if(urlPattern.matcher(source).find()){
                type = TypeSources.UNKNOWN;
            }else{
                Pattern localFilePattern = Pattern.compile("(\\.txt)$");
                if(localFilePattern.matcher(source).find()){
                    type = TypeSources.LOCAL_FILE;
                }
            }
        }

        return type;
    }

    void readAll(Parser parser) throws IOException{
        try {
            printer.println("Source is started  [" + source + "]", Printer.Level.DEBUG);
            String s;
            while (!badFlag.isFlag() && (s = parser.nextWord()) != null) {
                moveWord(s);
            }
        }
        catch (BadCharInputException | BadRussianWordException e) {
            catchExceptionMessage(e, "[" + source + "] Source is bad: " + e.getMessage());
        }
    }

    void moveWord(String s) {
        dict.accept(s);
    }

    void catchExceptionMessage(Exception e, String s){
        badFlag.setFlag(e, s);
    }

}






