package ru.babanin.task001.util;

/**
 * Ошибка поиска русских слов в заданных источниках
 */
public class SearcherUniqueException extends Exception{
    SearcherUniqueException(String s) {
        super(s);
    }
    SearcherUniqueException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
