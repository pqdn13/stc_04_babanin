package ru.babanin.task001.util;

import ru.babanin.task001.printer.Printer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Поисковик уникальных русских слов в заданных источниках
 */
public class SearcherUnique{
    private static Dictionary dict;

    static {
        dict = Dictionary.getInstance();
    }

    /**
     * Для каждого источника создается отдельный поток, где производится поиск.
     * Ожидает завершение всех потоков, после чего возвращает результат
     *
     * @param sources Список источников, где необходимо выполнить поиск русских слов
     * @return Список уникальный слов для всех источников
     * @throws SearcherUniqueException Ошибка, кидаемая в случае каких-либо ошибок чтения или обработки входного потока, не валидности источника
     */
    public static List<String> Search(String[] sources) throws SearcherUniqueException{
        BoxBadFlag badFlag = new BoxBadFlag();
        Printer printer = Printer.getInstance();

        List<Future> futures = new ArrayList<>(sources.length);
        ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();

        for (String source : sources) {
            SearcherInSource searcher;
            switch (SearcherInSource.GetTypeFileFromName(source)) {
                case LOCAL_FILE:
                    searcher = new SearcherInLocalFile(source, dict, badFlag);
                    break;
                case URL_FILE:
                    searcher = new SearcherInURLFile(source, dict, badFlag);
                    break;
                default:
                    throw new SearcherUniqueException("[" + source + "] unknown source");
            }

            if (searcher != null) {
                futures.add(threadPool.submit(searcher));
            }
        }

        for (Future future : futures) {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                String badMsg = "ThreadExecutor is bad: " + e.getMessage();
                badFlag.setFlag(e, badMsg);
                printer.println(badMsg, Printer.Level.ERROR);
            }
        }

        threadPool.shutdown();

        try {
            threadPool.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            String badMsg = "ThreadExecutor stop is bad: " + e.getMessage();
            printer.println(badMsg, Printer.Level.ERROR);
        }

        if (badFlag.isFlag()) {
            throw new SearcherUniqueException(badFlag.getMessage(), badFlag.getException());
        }

        List<String> uniques = dict.getUniqueWords();
        dict.reset();

        return uniques;
    }
}
