package ru.babanin.task001.util;

import ru.babanin.task001.printer.Printer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * Словарь, является синглтоном
 * принимает слова на вход слова, содержит их флаги уникальности
 */
class Dictionary implements Resetable, Consumer<String>{
    private Map<String, Boolean> map;
    private static Dictionary dict = null;
    private Printer printer;

    private Dictionary() {
        map = new ConcurrentHashMap<>();
        printer = Printer.getInstance();
    }

    static Dictionary getInstance(){
        if(dict == null){
            dict = new Dictionary();
        }
        return dict;
    }

    @Override
    public void accept(String word){
        Boolean val;
        if ((val = map.putIfAbsent(word, true)) != null && val) {
            print(word);
            map.put(word, false);
        }
    }

    private void print(String word){
        printer.println("[" + word + "] not unique", Printer.Level.DEBUG);
    }

    List<String> getUniqueWords(){
        List<String> list = new ArrayList<>();
        for (Map.Entry<String, Boolean> entry : map.entrySet()) {
            if(entry.getValue().equals(true)){
                list.add(entry.getKey());
            }
        }
        return list;
    }

    @Override
    public void reset() {
        map.clear();
    }
}
