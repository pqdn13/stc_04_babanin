package ru.babanin.task001.util;

import ru.babanin.task001.parser.Parser;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Consumer;

/**
 * Поисковик русских слов на удаленном файле,
 * располеженный по заданнуму url
 */
class SearcherInURLFile extends SearcherInSource{
    SearcherInURLFile(String source, Consumer<String> consumer, BoxBadFlag badFlag) {
        super(source, consumer, badFlag);
    }

    @Override
    public void run() {
        try(Parser parser = new Parser(new URL(source).openStream()))
        {
            readAll(parser);
        }
        catch (MalformedURLException e) {
            catchExceptionMessage(e, "[" + source + "] Source not found: " + e.getMessage());
        } catch (IOException e) {
            catchExceptionMessage(e, "[" + source + "] Input error: " + e.getMessage());
        }
    }
}