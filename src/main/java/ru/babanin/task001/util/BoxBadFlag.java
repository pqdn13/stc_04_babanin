package ru.babanin.task001.util;

/**
 * Объект которому единожды можно установить флаг,
 * может хранить сообщение и exception источника bad флага
 */
class BoxBadFlag {
    private boolean flag;
    private String message;
    private Exception exception;

    BoxBadFlag() {
        this.flag = false;
    }

    boolean isFlag() {
        return flag;
    }

    synchronized void setFlag(Exception exception, String message) {
        if(this.flag) return;

        this.flag = true;
        this.message = message;
        this.exception = exception;
    }

    String getMessage() {
        return message;
    }

    Exception getException() {
        return exception;
    }
}
