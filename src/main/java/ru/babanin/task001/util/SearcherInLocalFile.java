package ru.babanin.task001.util;

import ru.babanin.task001.parser.Parser;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.function.Consumer;

/**
 * Поисковик русских слов на локаном файле
 */
class SearcherInLocalFile extends SearcherInSource{
    SearcherInLocalFile(String source, Consumer<String> consumer, BoxBadFlag badFlag) {
        super(source, consumer, badFlag);
    }

    class ByteBufferInputStream extends InputStream{
        private final static long MAX_SIZE_CHUNK = 256 * 1024 * 1024;
        long size;
        long pos;
        MappedByteBuffer buf;
        FileChannel fileChannel;

        ByteBufferInputStream(String fileName) throws FileNotFoundException, IOException {
            fileChannel = new RandomAccessFile(new File(source), "r").getChannel();
            this.size = fileChannel.size();
            this.pos = 0;
        }

        private boolean readBuffer() throws IOException{
            if(pos >= size) return false;

            if ((size - pos) <= MAX_SIZE_CHUNK) {
                buf = fileChannel.map(FileChannel.MapMode.READ_ONLY, pos, size - pos);
                pos += size;
                fileChannel.close();
            }else {
                buf = fileChannel.map(FileChannel.MapMode.READ_ONLY, pos, MAX_SIZE_CHUNK);
                pos += MAX_SIZE_CHUNK;
            }
            return true;
        }

        public int read() throws IOException {
            if(buf == null || !buf.hasRemaining()){
                if(!readBuffer()) return -1;
            }
            return buf.get();
        }

        public int read(byte[] bytes, int off, int len) throws IOException {
            return -1;
        }

        @Override
        public void close() throws IOException {
            fileChannel.close();
        }
    }

    @Override
    public void run() {
        try (ByteBufferInputStream buf = new ByteBufferInputStream(source);
        Parser parser = new Parser(()->buf.read())) {
            readAll(parser);
        } catch (FileNotFoundException e) {
            catchExceptionMessage(e, "[" + source + "] Source not found: " + e.getMessage());
        } catch (IOException e) {
            catchExceptionMessage(e, "[" + source + "] Input error: " + e.getMessage() + "\n" + e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


