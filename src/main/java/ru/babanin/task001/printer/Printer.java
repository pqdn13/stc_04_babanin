package ru.babanin.task001.printer;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Синхронный принтер в консоль
 */
public class Printer implements  Runnable{
    private class Pair<T, E>{
        T first;
        E second;

        public Pair(T first, E second) {
            this.first = first;
            this.second = second;
        }
    }

    public enum Level{
        FATAL, ERROR, WARNING, DEBUG, TRACE
    }

    private static Printer printer;
    private static Thread thread;
    private Queue<Pair<String, Level>> queue = new ConcurrentLinkedQueue<>();

    private static final Logger LOGGER = Logger.getLogger(Printer.class);
    static {
        DOMConfigurator.configure("src/main/resources/log4j.xml");
    }


    private Printer() {}

    /**
     * @return Синглтон принтера
     */
    public static synchronized Printer getInstance(){
        if(printer == null){
            printer = new Printer();

            thread = new Thread(printer);
            thread.start();
        }
        return printer;
    }

    /**
     * Объявление о конце печати
     */
    public static void interrupt(){
        thread.interrupt();
    }

    /**
     * Ожидание конца печати всей очереди сообщений
     *
     * @throws InterruptedException Ошибка прерывания
     */
    public static void join() throws InterruptedException{
        thread.join();
    }

    /**
     * Печать в отдельном потоке
     *
     * @param s Строка помещаемая в очередь на печать
     */
    public synchronized void println(String s, Level level) {
        queue.add(new Pair<>(s, level));
    }

    @Override
    public void run() {
        while (true) {
            while (!queue.isEmpty()) {
                printToConsole(queue.poll());
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                break;
            }
        }

        while (!queue.isEmpty()) {
            printToConsole(queue.poll());
        }

    }

    private void printToConsole(Pair<String, Level> pair){
        switch (pair.second) {
            case DEBUG:
                LOGGER.debug(pair.first);
                break;
            case ERROR:
                LOGGER.error(pair.first);
                break;
            case FATAL:
                LOGGER.fatal(pair.first);
                break;
            case TRACE:
                LOGGER.trace(pair.first);
                break;
            case WARNING:
                LOGGER.warn(pair.first);
                break;
                default:
                    break;
        }

    }

}
