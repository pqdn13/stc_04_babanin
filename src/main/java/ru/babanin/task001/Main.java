package ru.babanin.task001;

import ru.babanin.task001.printer.Printer;
import ru.babanin.task001.util.SearcherUnique;
import ru.babanin.task001.util.SearcherUniqueException;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Printer printer = Printer.getInstance();
        List<String> list = null;

        try {
            list = SearcherUnique.Search(args);
            list.sort(String::compareTo);
        } catch (SearcherUniqueException e) {
            printer.println(e.getMessage(), Printer.Level.ERROR);
        }

        Printer.interrupt();
        try {
            Printer.join();
        } catch (InterruptedException e) {
            System.out.println("Sorry, print error");
        }

        if (list != null) {
            System.out.println("----Unique words:----");
            int count = 0;
            for (String s : list) {
                System.out.println(count + " " + s);
                count++;
            }
            System.out.println("---------------------");
        }else{
            System.out.println("Failed read!!!");
        }

    }
}
